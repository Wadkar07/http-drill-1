const http = require("http");
const url = require("url");
const { v4: uuidv4 } = require("uuid");
const fs = require("fs");
const PORT = process.env.PORT || 8000;

const server = http.createServer((req, res) => {
  let parsedURL = url.parse(req.url);
  let path = parsedURL.pathname;

  let route = typeof routes[path] !== "undefined" ? routes[path] : routes["notFound"];

  const urlElement = path.split("/");
  const param = urlElement[2];
  if (req.method === "GET") {
    if ((urlElement[1] === "status" || urlElement[1] === "delay") && urlElement.length === 3) {
      route = routes[urlElement[1]];
      route(res, param);
    }
    else {
      route(res);
    }
  }
  else {
    res.writeHead(400, { "Content-Type": "application/json" });
    res.end(JSON.stringify({ error: `http://${req.headers.host}${req.url} is not available with ${req.method} method` }, null, 4));
  }
});

server.listen(PORT, () => {
  console.log(`server started on port ${PORT}`);
});

let routes = {

  "/html": (res) => {
    fs.readFile("./index.html", "utf-8", (err, data) => {
      if (err) {
        console.error(err);
        res.writeHead(500, { "Content-Type": "application/json" });
        res.write(JSON.stringify({ error: err.message }, null, 4));
      }
      else {
        res.writeHead(200, { "Content-Type": "text/html" });
        res.write(data);
      }
      res.end();
    });
  },

  "/json": (res) => {
    fs.readFile('./object.json', 'utf-8', (err, data) => {
      if (err) {
        console.error(err);
        res.writeHead(500, { "Content-Type": "application/json" });
        res.write(JSON.stringify({ error: err.message }, null, 4));
      }
      else {
        res.writeHead(200, { "Content-Type": "application/json" });
        res.write(data);
      }
      res.end();
    });
  },

  "/uuid": (res) => {
    let data = {
      uuid: uuidv4()
    };
    let dataToString = JSON.stringify(data, null, 4);
    res.writeHead(200, { "Content-Type": "application/json" });
    res.write(dataToString);
    res.end();
  },

  status: (res, code) => {
    if (http.STATUS_CODES[code]) {
      let data = {
        [code]: http.STATUS_CODES[code]
      };
      res.writeHead(code, { "Content-Type": "application/json" });
      res.write(JSON.stringify(data, null, 4));
    }
    else {
      res.writeHead(400, { "Content-Type": "application/json" });
      res.write(JSON.stringify({ message: `${code} is not a invalid code` }, null, 4));
    }
    res.end();
  },

  delay: (res, sec) => {
    if (sec < 0 || Number.isNaN(Number(sec))) {
      res.writeHead(400, { "Content-Type": "application/json" });
      res.write(JSON.stringify({ error: `${sec} not a valid time in seconds` }, null, 4));
      res.end();
    }
    else {
      setTimeout(() => {
        res.write(`Displaying after ${sec} seconds`);
        res.end();
      }, sec * 1000);
    }
  },

  notFound: (res) => {
    let data = {
      error: "page not found",
      errorCode: 404,
    };
    res.writeHead(404, { "Content-Type": "application/json" });
    res.write(JSON.stringify(data, null, 4));
    res.end();
  },
};